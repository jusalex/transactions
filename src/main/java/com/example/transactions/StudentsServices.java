package com.example.transactions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StudentsServices {

    @Autowired
    private StudentRepository studentRepository;


    @Transactional(propagation = Propagation.REQUIRED)
    public Student create(Student student) {
        Student studentFromDB = this.getByName(student.getName());
        return studentRepository.save(student);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Student getByName(String name) {
        return studentRepository.findByName(name);
    }
}
