package com.example.transactions;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
class TransactionsApplicationTests {

	@Autowired
	private StudentsServices studentsServices;

	@Test
	public void create() {
		Student student = new Student();
		String test1 = "test1";
		student.setName(test1);
		student = studentsServices.create(student);
		assertThat(studentsServices.getByName(test1), is(notNullValue()));
		assertThat(student, is(notNullValue()));
	}

}
